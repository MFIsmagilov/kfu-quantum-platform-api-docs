[source,http,options="nowrap"]
----
include::../execute-program.adoc[]

{
    "countQubits": 3,
    "circuit": [
        {
            "operator": "CCNOT",
            "qubits":[0,1,2]
        }
    ]
}
----